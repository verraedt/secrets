#!/bin/bash

cd $(dirname $0)

PATH=bin/:$PATH

for ca in consul ldap radius sensu; do
  check-certs --skip $ca
  hieradata-cert $ca > hieradata/${ca}.crypted.yaml
done
